from django.db import models

# Create your models here.

class State(models.Model):
    name = models.CharField(max_length=50)
    description = models.TextField(default='')
    imagesmall = models.CharField(max_length=100, default='')
    imagelarge = models.CharField(max_length=100, default='')

    def __str__(self):
        return self.name

class Agent(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name

class Route(models.Model):
    price = models.FloatField()
    description = models.TextField(default='')
    duration = models.IntegerField()
    agent = models.ForeignKey(Agent)

    def __str__(self):
        return description

class Place(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField(default='')
    imagesmall = models.CharField(max_length=100, default='')
    imagelarge = models.CharField(max_length=100, default='')
    state = models.ForeignKey(State)

    def __str__(self):
        return self.name

class RoutePlace(models.Model):
    order = models.IntegerField()
    route = models.ForeignKey(Route)
    place = models.ForeignKey(Place)

class RouteDate(models.Model):
    date = models.DateField()
    route = models.ForeignKey(Route)