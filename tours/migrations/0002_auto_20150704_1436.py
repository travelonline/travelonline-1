# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tours', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='place',
            name='imagelarge',
            field=models.CharField(default='', max_length=100),
        ),
        migrations.AddField(
            model_name='place',
            name='imagesmall',
            field=models.CharField(default='', max_length=100),
        ),
        migrations.AddField(
            model_name='state',
            name='description',
            field=models.TextField(default=''),
        ),
        migrations.AddField(
            model_name='state',
            name='imagelarge',
            field=models.CharField(default='', max_length=100),
        ),
        migrations.AddField(
            model_name='state',
            name='imagesmall',
            field=models.CharField(default='', max_length=100),
        ),
        migrations.AlterField(
            model_name='place',
            name='description',
            field=models.TextField(default=''),
        ),
        migrations.AlterField(
            model_name='route',
            name='description',
            field=models.TextField(default=''),
        ),
    ]
